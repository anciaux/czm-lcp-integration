"""
This is a code to test piggybacking of the CZM model on the Akantu finite element code, based (but not exactly the same) on the plate test case.
"""

# Import the necessary modules
import os
import numpy as np
import akantu as aka


# Set the save folder
save_folder = "./testing/"

# Define the time steps (adaptive, and chosen somewhat arbitrarily for now)
h_max = 0.01
t = 0.0
t_max = 0.5
# Write the maximum time-step once cracking occurs
h_crack = 1.0e-4
# Set the θ parameter
θ = 0.5
# Set the applied displacement rate (in mm/ms)
applied_displacement_rate = 0.75

# Create a material, using the parameters from Berman et al. (2020), for PMMA.
# We will use units in N, mm, ms, g, MPa etc
E = 1000.0
ρ = 1.17E-3
ν = 0.25

# Create the CZM material (very weak for now)
# Critical traction (MPa), using value for average stress criterion
σ_c = 1.0
# Critical energy G_c (N/mm)
G_c = 0.25
# Critical traction ratio
γ = 1.0
# Critical normal opening length (mm)
δ_c_n = 2*G_c/σ_c
# Critical tangential displacement length (mm)
δ_c_t_2 = 2*G_c/(γ*σ_c)
# Newton's coefficient of restitution
e = 0.0

# Friction coefficient
μ = 0.3

# Tolerance condition
R = 1e-8
# Tolerance on β for the removal of one of last two lines of the LCP
tangent_R = 1e-3

# Set the threshold for insertion (fraction of σ_c), not used for now
insertion_threshold = 0.9

# Create the material file that Akantu will read in, using the plane strain assumption. Keeping the cohesive_linear model just for the time being while testing
material_data = """
model solid_mechanics_model_cohesive [
  cohesive_inserter [
    bounding_box = [[0,10],[-10, 10]]
  ]  # The region in which the cohesive elements can be inserted (the whole sample in this case)

  material elastic [
    name = PMMA
    rho = """ + str(ρ) + """  # density
    E  = """ + str(E) + """   # Young's modulus
    nu  = """ + str(ν) + """  # Poisson's ratio
    finite_deformation = false  # In the small deformation regime
  ]

  material cohesive_linear [
    name = cohesive
    sigma_c = """ + str(σ_c) + """  # critical traction
    G_c = """ + str(G_c) + """  # critical energy release rate
    beta = """ + str(1.) + """  # divisor of tangential traction for insertion check (TO BE SEEN IF NEEDS MODIFICATION FOR FRICTIONAL EFFECTS)
    penalty = 0. # Penalty for the contact (irrelevant, LCP will manage this)
  ]
]
"""
with open('material.dat', 'w') as f:
    f.write(material_data)

# We read in the material file that is created so Akantu can build the mass and stiffness matrices
aka.parseInput('material.dat')

# Create the mesh as a 2D mesh in Akantu
spatial_dimension = 2
mesh = aka.Mesh(spatial_dimension)
mesh.read('plate.msh')
# Get the coordinates of the nodes
node_coordinates = mesh.getNodes()

# Initialise the solid mechanics model that will understand the bulk material properties as well as the cohesion properties
model = aka.SolidMechanicsModelCohesive(mesh)
# Call the explicit lumped mass method so that the velocities are initialised (despite this call, the mass matrix below remains consistent rather than lumped, the effect of the call is just to allow us to extract the terms we need, we will do the management of the system ourselves (notably, not explicit integration!))
model.initFull(_analysis_method=aka._explicit_lumped_mass, _is_extrinsic=True)
# Initilize a new solver (explicit Newmark with lumped mass)
model.initNewSolver(aka._explicit_lumped_mass)
# Build the stiffness and consistent mass matrices and expose them to this code so we can build the LCP
model.assembleStiffnessMatrix()
model.assembleMass()
K = aka.AkantuSparseMatrix(model.getDOFManager().getMatrix('K')).toarray()
M = aka.AkantuSparseMatrix(model.getDOFManager().getMatrix('M')).toarray()
# Dynamic insertion of cohesive elements
model.updateAutomaticInsertion()
# Get the number of degrees of freedom
n_dof = np.shape(K)[0]

"""Verifying that I can get the normal and tangent vectors which will be needed for LCP construction, not used for the moment"""
# Get the mesh facets and connectivities
mesh_facets = mesh.getMeshFacets()
connectivity_facets = mesh_facets.getConnectivities()
# Get the cohesive inserter and check facets
cohesive_inserter = model.getElementInserter()
check_facets = cohesive_inserter.getCheckFacets()
# Loop by all facet types used in the simulation
for facet_type in connectivity_facets.elementTypes(dim=(spatial_dimension - 1)):
    # Now we get a list of the nodes lying on the facets of the given type
    connectivity_facet_type = connectivity_facets(facet_type)
    # Check the facets (this returns a Boolean list, presumably of whether the cohesive traction threshold has been exceeded)
    check_facet_type = check_facets(facet_type)
    for el, connectivity_facet_type in enumerate(connectivity_facet_type):
        # Check the direction of the vector that goes in the clockwise direction of the facet
        tangent_vector = (node_coordinates[connectivity_facet_type[1],
                          :] - node_coordinates[connectivity_facet_type[0], :])
        tangent_unit_vector = (tangent_vector/np.linalg.norm(tangent_vector))
        # Get what *should* be the outwares pointing normal unit vector
        normal_unit_vector = np.array(
            [tangent_unit_vector[1], -tangent_unit_vector[0]])


# Set the initial time step and build the augmented mass matrix
h = h_max
M_hat = M + ((θ*h)**2)*K

# Set the Dirichlet boundary conditions, fixing the displacement on the left and bottom, in the directions normal to those boundaries
model.applyBC(aka.FixedValue(0., aka._x), 'left')
model.applyBC(aka.FixedValue(0., aka._y), 'bottom')

# Akantu blocked dofs returns on a per-node basis, so we need to flatten it
control = model.getBlockedDOFs().flatten('C')


# Define a function which inputs the augmented mass matrix, the free-flight impulse, the velocity and the control vector, to enforce the boundary conditions
def boundary_condition_enforcement(M_hat, i_hat, v, control):
    i_hat_bar = i_hat.copy()
    # Loop through the controlled velocities
    for i in range(np.shape(v)[0]):
        # If it's a controlled velocity, calculate the impulse from the augmented mass matrix.
        if control[i]:
            i_hat_bar[i] = M_hat[i, i]*v[i]
            # Then subtract its effect from all the other entries
            for j in range(np.shape(M_hat)[0]):
                if j != i:
                    # Make sure we don't interfere with any of the other controlled
                    # velocities
                    if not control[j]:
                        i_hat_bar[j] = i_hat_bar[j] - M_hat[j, i]*v[i]
    return i_hat_bar


# Define a function that modifies the augmented mass matrix to enforce the boundary conditions (we will only need to call this once, hence why it's a seperate function)
def modified_augmented_mass_matrix(M_hat, control):
    M_hat_bar = M_hat.copy()
    # Loop through the controlled velocities
    for i in range(np.shape(control)[0]):
        # If it's a controlled velocity, change the matrx
        if control[i]:
            # Reset the augmented mass matrix
            M_hat_bar[i, :] = 0.0
            M_hat_bar[:, i] = 0.0
            M_hat_bar[i, i] = M_hat[i, i]
    return M_hat_bar


# Create M_hat_bar
M_hat_bar = modified_augmented_mass_matrix(M_hat, control)
# Invert it
M_hat_inv = np.linalg.inv(M_hat_bar)


# Define the force function
def force_func(t):
    return 0.5*t


# Initialization for bulk vizualisation
model.setBaseName('plate')
model.addDumpFieldVector('displacement')
model.addDumpFieldVector('velocity')
model.addDumpFieldVector('external_force')
model.addDumpField('strain')
model.addDumpField('stress')
dof_manager = model.getDOFManager()

# Run an integration of the elastic system
while t < t_max:
    # Neumann traction applied at the top of the plate
    F = force_func(t)
    traction = np.zeros(spatial_dimension)
    traction[int(aka._y)] = F
    model.getExternalForce()[:] = 0
    model.applyBC(aka.FromSameDim(traction), 'top')
    # Recover the force vector
    F_vec = model.getExternalForce().flatten('C')
    # Recover the velocity and displacement vectors
    v = model.getVelocity().flatten('C')
    u = model.getDisplacement().flatten('C')
    # Compute the free-flight impulse
    if t > 0:
        i_hat = (M - (h**2)*θ*(1 - θ)*K) @ v - h * \
            K @ u + h*(θ*F_vec + (1 - θ)*F_vec_old)
    else:
        i_hat = (M - (h**2)*θ*(1 - θ)*K) @ v - h*K @ u + h*θ*F_vec
        v_vec_old = v.copy()
    # Calculate i_hat_bar that enforces the boundary conditions
    i_hat_bar = boundary_condition_enforcement(M_hat, i_hat, v, control)
    # Calculate the velocities and displacements, then  return them to the Akantu model with the appropriate shape
    v = M_hat_inv @ i_hat_bar
    u = u + h*(θ*v + (1 - θ)*v_vec_old)
    model.getVelocity()[:] = np.reshape(v, np.shape(model.getVelocity()))
    model.getDisplacement()[:] = np.reshape(
        u, np.shape(model.getDisplacement()))
    # G: This actually calculates the stresses from the model
    dof_manager.zeroResidual()
    model.assembleResidual()
    # G: This gets a numpy view on the stress
    σ = model.getMaterial(0).getStress(aka._triangle_3)
    """Similarly, if I try and get the strain, it doesn't work. Creating matrices of nabal_q and u of the right shapes (I think) and calling the gradientOnIntegrationPoints method as below doesn't work (it returns the error akantu.py11_akantu.Exception: akantu::debug::Exception : cannot resize a temporary array [../../../python/py_aka_array.hh:80]

    G: ok this is a bug currently fixed in a merge request. It should go through the pipes early next week. Can you tell me why do you need to access the gradients ?
    
    nabla_q = np.zeros(np.shape(σ))
    u_reshape = np.reshape(u, (2, int(n_dof/2)))
    model.getFEEngine().gradientOnIntegrationPoints(u_reshape, nabla_q, n_dof, aka._triangle_3)
    """
    # Store the old force and velocity vectors
    v_vec_old = v.copy()
    F_vec_old = F_vec.copy()
    """If I call model.solveStep() here, it will work and write non-zero stresses (however the strains are still zero). However, the velocities are different to what I have calculated above, so I can't use it as it will interefer with the future LCP construction.

    G: yes I understand. You should not call the solvestep.
    """
    # model.solveStep()
    # Dump the results
    model.dump()
    # Update the time
    t = t + h


# Delete the files if they exist, to allow for the new ones to be written
# os.system("rm -r " + save_folder + "paraview")
# Move the Paraview files to the save folder
# os.system("mv paraview " + save_folder)
